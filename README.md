# .NET Core + EF Core + SQLite provider
This walkthrough will create a simple console application using .NET Core and the SQLite provider.

## Prerequisites

**Minimum system requirements**
> Ubuntu, Debian or one of their derivatives

## Caution

**Known Issues**

* Migrations on SQLite do not support more complex schema changes due to limitations in SQLite itself. See SQLite Limitations

* On Linux, Microsoft.EntityFrameworkCore.Sqlite uses the operating system’s shared SQLite library, libsqlite3. This may not be installed by default.

> On Ubuntu 14, install the SQLite library with apt-get.

>> sudo apt-get install libsqlite3-dev

> On RedHat, install the SQLite library with yum.

>> sudo yum install sqlite-devel

